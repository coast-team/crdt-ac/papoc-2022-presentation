% !TeX document-id = {55e50444-9a36-43a3-ab14-e161b5764519}
% !TeX TXS-program:compile = txs:///pdflatex/[--shell-escape]

\documentclass[10pt]{beamer}
\usepackage[utf8]{inputenc}

\usetheme[progressbar=frametitle]{metropolis}

\usepackage{acronym} % \ac[p], \acl[p], \acs[p], \acf[p]
\usepackage{appendixnumberbeamer}
\usepackage{booktabs} % \toprule, \midrule, \cmidrule, \bottomrule
\usepackage[scale=2]{ccicons}
\usepackage{subcaption} % subfigure
\usepackage[absolute, overlay]{textpos}
\usepackage{tikz}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.misc}
\usepackage{verbatim}

\usepackage{transparent}

\usepackage{graphicx}
\usepackage{color}
\AtBeginDocument{
\definecolor{pdfurlcolor}{rgb}{0,0,0}
\definecolor{pdfcitecolor}{rgb}{0,0,0}
\definecolor{pdflinkcolor}{rgb}{0,0,0}
\definecolor{light}{gray}{.85}
\definecolor{vlight}{gray}{.95}
\definecolor{darkgreen}{RGB}{77,172,38}
\definecolor{darkblue}{RGB}{5,113,176}
\definecolor{mydarkblue}{RGB}{116,173,209}
\definecolor{mydarkblueid}{RGB}{83,154,198}
\definecolor{mylightblue}{RGB}{171,217,233}
\definecolor{mydarkorange}{RGB}{244,109,67}
\definecolor{mylightorange}{RGB}{252,153,54}
\definecolor{mydarkred}{RGB}{215,48,39}
\definecolor{mydarkpurple}{RGB}{140,107,177}
\definecolor{mydarkpurpleid}{RGB}{136,86,167}
}

% Acronyms
% --------
\acrodef{CRDT}[CRDT]{Conflict-free Replicated Data Type}
\acrodefplural{CRDT}[CRDTs]{Conflict-free Replicated Data Types}

\author{
  \textbf{Pierre-Antoine Rault} (\url{pierre-antoine.rault@loria.fr})
  \\
  Claudia-Lavinia Ignat, Olivier Perrin
  \\
}
\title{Access Control and Security Mechanisms in Distributed Systems with no Central Authority}
\subtitle{Policy CRDTs for distributed access control}
\institute{
  \vspace{3em}
  \hspace{4em}
  \includegraphics[width=2.8cm]{images/loria-logo.png}\hspace{3em}
  \includegraphics[width=1.2cm]{images/ul-logo.pdf}\hspace{3em}
  \includegraphics[width=3cm]{images/inria-logo.pdf}\hspace{3em}
%  \includegraphics[width=1.2cm]{images/cnrs-logo.png}
}

\usepackage[backend=bibtex,doi=false,isbn=false,defernumbers=true,bibstyle=authoryear,sorting=ydnt,maxbibnames=1,maxcitenames=1]{biblatex}
\addbibresource{biblio.bib}
\AtBeginBibliography{\footnotesize}
\AtEveryCitekey{\clearlist{institution}
	\clearfield{note}
	\clearfield{pages}
	\clearlist{location}
	\clearlist{publisher}
	\clearname{editor}
	\clearfield{type}}

\renewcommand{\thefootnote}{[\arabic{footnote}]}
\newcommand{\trm}[1]{\mathit{#1}}
\newcommand{\id}[3]{$\trm{#1}^{\trm{#2}}_{\trm{#3}}$}
\newcommand{\epoch}[1]{$\varepsilon_{#1}$}


\newcommand{\widthletter}{2em}
\newcommand{\widthblock}{3em}
\newcommand{\widthoriginepoch}{1.65em}
\newcommand{\widthepoch}{1.8em}

% Tikz styles
\tikzset{
    common/.style={anchor=west, draw, rectangle, minimum height=6mm},
    letter/.style={common, minimum width=\widthletter},
    block/.style={common, minimum width=\widthblock},
    epoch/.style={letter, rounded rectangle, rounded rectangle east arc=0pt, minimum width=\widthepoch},
    point/.style={insert path={ node[scale=5*sqrt(\pgflinewidth)]{.} }},
    op/.style={draw, circle, minimum size=2.7em},
    cross/.style={
        path picture={
            \draw[mydarkred, very thick]
                (path picture bounding box.south east)--(path picture bounding box.north west)
                (path picture bounding box.south west)--(path picture bounding box.north east);
        }
    },
    invisible/.style={opacity=0},
    visible on/.style={alt=#1{}{invisible}},
    alt/.code args={<#1>#2#3}{%
      \alt<#1>{\pgfkeysalso{#2}}{\pgfkeysalso{#3}} % \pgfkeysalso doesn't change the path
    },
}

% LaTeX Overlay Generator - Annotated Figures v0.0.1
% Created with http://ff.cx/latex-overlay-generator/
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\annotatedFigureBoxCustom{bottom-left}{top-right}{label}{label-position}{box-color}{label-color}{border-color}{text-color}
\newcommand*\annotatedFigureBoxCustom[8]{\draw[#5,thick,rounded corners] (#1) rectangle (#2);\node at (#4) [fill=#6,thick,shape=circle,draw=#7,inner sep=2pt,font=\sffamily,text=#8] {\textbf{#3}};}
%\annotatedFigureBox{bottom-left}{top-right}{label}{label-position}
\newcommand*\annotatedFigureBox[4]{\annotatedFigureBoxCustom{#1}{#2}{#3}{#4}{red}{white}{black}{black}}
\newcommand*\annotatedFigureText[4]{\node[draw=none, anchor=south west, text=#2, inner sep=0, text width=#3\linewidth,font=\sffamily] at (#1){#4};}
\newenvironment {annotatedFigure}[1]{\centering\begin{tikzpicture}
		\node[anchor=south west,inner sep=0] (image) at (0,0) { #1};\begin{scope}[x={(image.south east)},y={(image.north west)}]}{\end{scope}\end{tikzpicture}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage{silence}
\WarningFilter{biblatex}{Patching footnotes failed}

%% Frame logos
\newenvironment{winter}[1]{%
	\setbeamercolor*{frametitle}{bg=cyan}
	\addtobeamertemplate{frametitle}{}{%
		\begin{tikzpicture}[remember picture,overlay]
            \tikzset{every node}=[font=\footnotesize\sffamily]
			\node[anchor=north east,yshift=-2pt,align=right] at (current page.north east) {%
				WINTER\\SCHOOL
			};
	\end{tikzpicture}}
	\begin{frame}[environment=winter,fragile]{#1}
	}{
	\end{frame}
}

\usepackage[pdf]{graphviz}

\begin{document}

\begin{frame}[t,plain]
  \maketitle
\end{frame}

\begin{frame}{Modern applications}
  Modern applications are ubiquitous and relied on by users exchanging around common content:
  
  \begin{itemize}
 	  	\item Chat: Signal, WhatsApp, etc.
 	  	\item Videoconferencing: Zoom, BigBlueButton, etc.
 	  	\item Collaborative editing: Google Doc, Etherpad, HedgeDoc, CryptPad, etc.
 	  	\item File Systems: Ceph, GlusterFS, Tahoe-LAFS, etc.
 	  	\item Code collaboration: GitHub, GitLab, Gitea, etc.
 	  	\item Scientific collaboration: OpenConf, EasyChair, etc.
  \end{itemize}
\end{frame}

\begin{frame}{Modern applications and their challenges}
	Modern applications are often dependent on centralized services for their operation:
	
	\begin{itemize}
		\item For distribution and content availability
		\item For authentication and access control
		\item For security and confidentiality
	\end{itemize}
\end{frame}

\begin{frame}{\acfp{CRDT}\footfullcite{shapiro2011}}
	\begin{columns}
		\begin{column}{0.65\textwidth}
			\begin{figure}
				\centering
\begin{annotatedFigure}
	{\includegraphics[height=6cm]{images/semilattice.pdf}}
\annotatedFigureBox{0.01,0.189}{0.32,0.756}{S1}{0.01,0.756}%tl
\annotatedFigureBox{0.3735,0.19}{0.7111,0.755}{S2}{0.3735,0.755}%tl
\annotatedFigureBox{0.7452,0.191}{1.0623,0.756}{S3}{0.7452,0.756}%tl
\end{annotatedFigure}

				\caption{Document editions converge via CRDT}
			\end{figure}
		\end{column}
		\begin{column}{0.35\textwidth}
			\begin{itemize}
				\item Replicated data structure
				\item Updates performed without coordination
				\item Strong Eventual Consistency
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Access Control for CRDTs}
	\begin{columns}
		\begin{column}{0.65\textwidth}
			\begin{figure}
				\centering
				\begin{annotatedFigure}
					{\includegraphics[height=6cm]{images/semilattice_only_circles.pdf}}
\annotatedFigureBox{0.01,0.189}{0.32,0.756}{S1}{0.01,0.756}%tl
\annotatedFigureBox{0.3735,0.19}{0.7111,0.755}{S2}{0.3735,0.755}%tl
\annotatedFigureBox{0.7452,0.191}{1.0623,0.756}{S3}{0.7452,0.756}%tl
				\end{annotatedFigure}
				\caption{Split convergence when the rightmost member only has access to reading and editing circle shapes}
			\end{figure}
		\end{column}
		\begin{column}{0.35\textwidth}
			\begin{itemize}
				\item<1-> Replicated Access Policy
				\item<2-> Operations are only transmitted to members in capacity
				\item<3-> Operations to be encrypted per read access (i.e. S3)
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}[standout]
  \alert{Approach}

  \bigskip

  Systematic study of pairwise conflicts resolutions for a novel, adaptable Policy CRDT
\end{frame}

\begin{frame}{Approach constraints}
	
	\begin{itemize}
		\item \textbf{Multiple admins:} multiple administrators can concurrently edit an access control policy, potentially causing conflicts
		\item \textbf{Each application has different conflicts:} different systems have different meanings tied to each right, and generate different conflicts
		\item \textbf{Dynamic groups:} some rights are tied to the ability to read parts of the document, and must be mapped to new group keys for encryption as they change
	\end{itemize}
\end{frame}

\begin{frame}{Conflict between a policy operation and a data operation}
	\begin{figure}
		\centering
		\begin{overprint}
			\onslide<1>\includegraphics[height=6cm]{images/Policy-Document 01.png}
      		\onslide<2>\includegraphics[height=6cm]{images/Policy-Document 02.png}
       		\onslide<3>\includegraphics[height=6cm]{images/Policy-Document 03.png}
		\end{overprint}
	\end{figure}

	\small{A = right to administer, R = right to read, W = right to edit/write}
\end{frame}

\begin{frame}[standout]
	What about policy-to-policy conflicts?
\end{frame}

\begin{frame}{Conflict between two policy operations}
	\begin{figure}
		\centering
		\begin{overprint}
			\onslide<1>\includegraphics[height=6cm]{images/Policy-Policy 01.png}
			\onslide<2>\includegraphics[height=6cm]{images/Policy-Policy 02.png}
			\onslide<3>\includegraphics[height=6cm]{images/Policy-Policy 03.png}
		\end{overprint}
	\end{figure}

	\small{A = right to administer, R = right to read, W = right to edit/write}
\end{frame}

\begin{frame}{Limitations/Trade-offs}
  \begin{block}{Subjective}
  	\begin{itemize}
  		\item Strategies have to be tailored per application
  		\item There is no single way to resolve a conflict while ensuring integrity and confidentiality\footfullcite{yanakieva2021a}
  	\end{itemize}
  \end{block}

  \pause
	
  \begin{block}{Monolithic}
    \begin{itemize}
      \item No rights can be added once the application runs
      \item All policy operations have to be handled by the same CRDT
    \end{itemize}
  \end{block}

  \pause
  
  \begin{block}{Requires \textit{Undo} and \textit{Redo}}
  	\begin{itemize}
      \item Loss of monotonicity (absence of rollbacks)
	\end{itemize}
  \end{block}

%  \begin{block}{In practice we scope commutativity}
%  	\begin{itemize}
%		\item we limit commutativity of document operations to a known policy context
%		\item it allows processing branches that need to be dropped upon policy-policy without a costly version vector
%  	\end{itemize}
%  \end{block}
\end{frame}

\begin{frame}[standout]
  Dealing with multiple concurrent policy and data conflicts
\end{frame}

\begin{frame}{Handling concurrent operations}

  \begin{figure}
%	\begin{tikzpicture}[remember picture]
%		\node[anchor=south west,inner sep=0] (image) at (0,0) {\includegraphics[height=2cm]{images/epoch-hierarchy.png}};
%		\begin{scope}[x={(image.south east)},y={(image.north west)}]
%			\draw[red,ultra thick,rounded corners] (0.55,1) rectangle (0.78,0.75);
%			\draw[help lines,xstep=.1,ystep=.1] (0,0) grid (1,1);
%			\foreach \x in {0,1,...,9} { \node [anchor=north] at (\x/10,0) {0.\x}; }
%			\foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y/10) {0.\y}; }
%		\end{scope}
%	\end{tikzpicture}

	\begin{annotatedFigure}
		{\includegraphics[height=2cm]{images/epoch-hierarchy.png}}
		\uncover<3>{				
			\annotatedFigureBoxCustom{0.346,0.5546}{0.8823,1.0232}{A}{0.8823,1.0232}{red}{red}{red}{white}
			\annotatedFigureBoxCustom{0.346,-0.001}{0.8823,0.4676}{B}{0.8823,-0.001}{green}{green}{green}{white}
		}
	\end{annotatedFigure}

	\only<1>{\caption{\footnotesize Conflicts between two epochs (here $e_{11}$ and $e_{12}$) affects operations referencing them (OP5 and OP3)}}
  \end{figure}

\only<1>{
  \begin{block}{Track concurrency via \textit{epochs}\footfullcite{nicolas2020}}
  	\begin{itemize}
      \item each policy operation can be referred to by subsequent operations of any type
      \item each operation must refer to the most recent policy operation

  	\end{itemize}
  \end{block}
}

\only<2>{
  \begin{block}{Advantages of \textit{epochs}}
  	\begin{itemize}
      \item only policy operations up to a commonly seen ancestor need to be kept track of
      \item operations could select a reference closer to their semantic dependency, allowing for finer conflict resolutions
  	\end{itemize}
  \end{block}
}

\only<3>{
  \begin{block}{Scope concurrency to policy operations}
  	\begin{itemize}
  	  \item data operations are undone and redone following the \textit{epoch} they reference\footnote{here if OP2 wins in a conflict with OP4 \textit{undoes} $A$ and \textit{redoes} $B$ where needed}
      \item reduces proof of convergence to proving policy conflicts can be resolved
  	\end{itemize}
  \end{block}
}
\end{frame}

\begin{frame}{From ancestor tracking to causality tracking}
%	\digraph[height=4cm]{abc}{
%		rankdir=LR;
%		p1 -> p2 -> p4;
%		p1 -> p3 -> p4;
%	}
	
	\begin{block}{Limits of \textit{epochs}}
		\begin{itemize}
			\item only partial ancestry tracking: need for version vectors dependencies
			\item no causality tracking: need for semantic dependencies
		\end{itemize}
	\end{block}
	In its current form, it detects more conflicts than necessary.
\end{frame}

\begin{frame}{Conclusion}
  \begin{block}{Done}
    \vspace{-1mm}
    \begin{itemize}
      \item Design key parts of a new Policy \ac{CRDT}
      \item Validate their resolution strategies through systemic exploration in the general \& Google Doc cases
      \item Adapt conflict resolution strategies to key scenarios (i.e. POSIX, which requires the notion of groups)
    \end{itemize}
  \end{block}

  \pause

  \begin{block}{Work in progress}
    \vspace{-1mm}
    \begin{itemize}
      \item Adapt causality tracking mecanisms to our CRDT
      \item Adapt undo/redo mechanisms to our CRDT
    \end{itemize}
  \end{block}

  \pause

  \begin{block}{Future work}
    \vspace{-1mm}
    \begin{itemize}
      \item Tie policy changes to an underlying key agreement group change
%      \item Implementing support in MUTE \footfullcite{nicolas2017}, our P2P collaborative text editor
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[standout]
  Thank you for your attention
  \vspace{3em}
  \begin{center}
    \ccby
  \end{center}
\end{frame}

\end{document}
